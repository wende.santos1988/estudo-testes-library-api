package com.wendel.library.utils;

import com.wendel.library.api.model.entity.Book;
import com.wendel.library.api.model.entity.Loan;

import java.time.LocalDate;

public class LoanUtils {

    public static Loan LoanWithId() {
        return Loan.builder().id(1L).customer("Wendel").loanDate(LocalDate.now()).book(BookUtils.bookWithId()).build();
    }

    public static Loan LoanWithIdAndBook(Book book) {
        return Loan.builder().id(1L).customer("Wendel").book(book).loanDate(LocalDate.now()).build();
    }

}
