package com.wendel.library.utils;

import com.wendel.library.api.model.entity.Book;

public class BookUtils {

    public static Book createNewBook() {
        return Book.builder().title("Meu Livro").author("autor").isbn("123").build();
    }

    public static Book bookWithId() {
        return Book.builder().id(1L).title("Meu Livro").author("autor").isbn("123").build();
    }

    public static Book bookWithIdToUpdate() {
        return Book.builder().id(1L).title("some title").author("some author").isbn("123").build();
    }

}
