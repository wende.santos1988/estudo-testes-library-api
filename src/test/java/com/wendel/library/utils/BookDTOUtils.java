package com.wendel.library.utils;

import com.wendel.library.api.model.dto.BookDTO;
import com.wendel.library.api.model.entity.Book;

public class BookDTOUtils {

    public static BookDTO createNewBookDTO() {
        return BookDTO.builder().title("Meu Livro").author("autor").isbn("123").build();
    }

    public static BookDTO bookWithIdToUpdate() {
        return BookDTO.builder().id(1L).title("some title").author("some author").isbn("112233").build();
    }

}
