package com.wendel.library.api.repository;

import com.wendel.library.api.model.entity.Book;
import com.wendel.library.utils.BookUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("teste")
@DataJpaTest
public class BookRepositoryTest {

    @Autowired
    TestEntityManager entityManager;

    @Autowired
    BookRepository repository;

    @Test
    @DisplayName("should be return true when exist book with isbn informed ")
    public void shouldBeReturnTrueWhenExistBookWithIsbnInformed() {
        // cenario
        String isbn = "123";
        Book book = BookUtils.createNewBook();
        book.setIsbn(isbn);
        entityManager.persist(book);

        // execucao
        Boolean exists = repository.existsByIsbn(isbn);

        // verificacao
        assertThat(exists).isTrue();
    }

    @Test
    @DisplayName("should be return false when  isbn informed doesn't exist")
    public void shouldBeReturnFalseWhenIsbnInformedDoesntExist() {
        // cenario
        String isbn = "123";

        // execucao
        Boolean exists = repository.existsByIsbn(isbn);

        // verificacao
        assertThat(exists).isFalse();
    }

    @Test
    @DisplayName("should get a book by id")
    public void shouldGetABookById() {
        // cenario
        Book book = BookUtils.createNewBook();
        entityManager.persist(book);

        // execucao
        Optional<Book> foundBook = repository.findById(book.getId());

        // verificacao
        assertThat(foundBook.isPresent()).isTrue();

    }

    @Test
    @DisplayName("should save a book")
    public void shouldSaveABook() {
        Book book = BookUtils.createNewBook();

        Book savedBook = repository.save(book);

        assertThat(savedBook.getId()).isNotNull();
    }

    @Test
    @DisplayName("should delete a book")
    public void shouldDeleteABook() {

        // cenario
        Book book = BookUtils.createNewBook();
        entityManager.persist(book);
        Book foundBook = entityManager.find(Book.class, book.getId());

        // execucao
        repository.delete(foundBook);

        // verificacao
        Book deletedBook = entityManager.find(Book.class, book.getId());
        assertThat(deletedBook).isNull();

    }

}
