package com.wendel.library.api.service;

import com.wendel.library.api.model.entity.Book;
import com.wendel.library.api.repository.BookRepository;
import com.wendel.library.api.service.impl.BookService;
import com.wendel.library.exceptions.BusinessException;
import com.wendel.library.utils.BookUtils;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class BookServiceTest {

    IBookService service;

    @MockBean
    BookRepository repository;

    @BeforeEach
    public void setUp() {
        this.service = new BookService(repository);
    }

    @Test
    @DisplayName("should save a book")
    public void shouldSaveABook() {
        //Cenario
        Book book = Book.builder().title("Nárnia").author("CS Lewis").isbn("112233").build();
        when(repository.existsByIsbn(book.getIsbn())).thenReturn(false);
        when(repository.save(book))
                .thenReturn(Book.builder()
                    .id(11L)
                    .title("Nárnia")
                    .author("CS Lewis")
                    .isbn("112233")
                    .build());

        //Execução
        Book savedBook = service.save(book);

        //Verificação
        assertThat(savedBook.getId()).isNotNull();
        assertThat(savedBook.getIsbn()).isEqualTo("112233");
        assertThat(savedBook.getTitle()).isEqualTo("Nárnia");
        assertThat(savedBook.getAuthor()).isEqualTo("CS Lewis");
    }

    @Test
    @DisplayName("should throw a business error when trying to create a book with duplicate isbn")
    public void shouldThrowABusinessErrorWhenTryingToCreateABookWithDuplicateIsbn() {
        // cenario
        Book book = BookUtils.createNewBook();
        when(repository.existsByIsbn(book.getIsbn())).thenReturn(true);

        // execucao
        Throwable exception = Assertions.catchThrowable(() -> service.save(book));

        // verificacao
        assertThat(exception)
                .isInstanceOf(BusinessException.class)
                .hasMessage("ISBN já cadastrado.");

        Mockito.verify(repository, Mockito.never()).save(book);
    }

    @Test
    @DisplayName("should get a book by id")
    public void shouldGetABookById() {

        // cenario
        Long id = 1L;
        Book book = BookUtils.bookWithId();
        book.setId(id);
        when(repository.findById(id)).thenReturn(Optional.of(book));

        // execucao
        Optional<Book> foundBook = service.getById(id);

        // verificacoes
        assertThat(foundBook.isPresent()).isTrue();
        assertThat(foundBook.get().getId()).isEqualTo(id);
        assertThat(foundBook.get().getAuthor()).isEqualTo(book.getAuthor());
        assertThat(foundBook.get().getTitle()).isEqualTo(book.getTitle());
        assertThat(foundBook.get().getIsbn()).isEqualTo(book.getIsbn());

    }

    @Test
    @DisplayName("should return empty when trying to get a book that doesn't exist in the database")
    public void shouldReturnEmptyWhenTryingToGetABookThatDoesntExistInTheDatabase() {

        // cenario
        Long id = 1L;

        when(repository.findById(id)).thenReturn(Optional.empty());

        // execucao
        Optional<Book> book = service.getById(id);

        // verificacoes
        assertThat(book.isPresent()).isFalse();
    }

    @Test
    @DisplayName("should return empty when delete a book")
    public void shouldReturnEmptyWhenDeleteABook() {
        // cenario
        Book book = BookUtils.bookWithId();

        // execucao
        org.junit.jupiter.api.Assertions.assertDoesNotThrow(() -> service.delete(book));

        // verificacao
        Mockito.verify(repository, Mockito.times(1)).delete(book);

    }

    @Test
    @DisplayName("should error occur when trying to delete nonexistent book")
    public void shouldErrorOccurWhenTryingToDeleteNonexistentBook() {
        Book book = new Book();

        // execucao
        org.junit.jupiter.api.Assertions.assertThrows(IllegalArgumentException.class, () -> service.delete(book));

        // verificacao
        Mockito.verify(repository, Mockito.never()).delete(book);
    }

    @Test
    @DisplayName("should return updated object o when delete a book")
    public void shouldReturnUpdatedObjectWhenDeleteABook() {
        // cenario
        Long id = 1L;
        Book updatingBook = Book.builder().id(id).build();

        // simular resultado do update
        Book updatedBook = BookUtils.bookWithIdToUpdate();
        updatedBook.setId(id);

        when(repository.save(updatingBook)).thenReturn(updatedBook);

        // execucao
        Book book = service.update(updatingBook);

        // verificacao
        assertThat(book.getId()).isEqualTo(updatedBook.getId());
        assertThat(book.getTitle()).isEqualTo(updatedBook.getTitle());
        assertThat(book.getAuthor()).isEqualTo(updatedBook.getAuthor());
        assertThat(book.getIsbn()).isEqualTo(updatedBook.getIsbn());

    }

    @Test
    @DisplayName("should error occur when trying to update nonexistent book")
    public void shouldErrorOccurWhenTryingToUpdateNonexistentBook() {
        Book book = new Book();

        // execucao
        org.junit.jupiter.api.Assertions.assertThrows(IllegalArgumentException.class, () -> service.update(book));

        // verificacao
        Mockito.verify(repository, Mockito.never()).save(book);
    }

    @Test
    @DisplayName("should get a paginated book")
    @SuppressWarnings("unchecked")
    public void shouldGetAPaginatedBook() {
        // cenario
        Book book = BookUtils.bookWithId();

        PageRequest pageRequest = PageRequest.of(0, 10);

        List<Book> books = Collections.singletonList(book);

        Page<Book> page = new PageImpl<>(books, pageRequest, 1);

        when(repository.findAll(Mockito.any(Example.class), Mockito.any(PageRequest.class)))
            .thenReturn(page);

        // execucao
        Page<Book> result = service.find(book, pageRequest);

        // verificacoes
        assertThat(result.getTotalElements()).isEqualTo(1);
        assertThat(result.getContent()).isEqualTo(books);
        assertThat(result.getPageable().getPageNumber()).isEqualTo(0);
        assertThat(result.getPageable().getPageSize()).isEqualTo(10);
    }

    @Test
    @DisplayName("should to get a book by isbn")
    public void shouldToGetABookByIsbn() {
        String isbn = BookUtils.createNewBook().getIsbn();
        Book bookReturn = Book.builder().id(1L).isbn(isbn).build();

        when(repository.findByIsbn(isbn)).thenReturn(Optional.of(bookReturn));

        Optional<Book> book = service.getBookByIsbn(isbn);
        
        assertThat(book.isPresent()).isTrue();
        assertThat(book.get().getId()).isEqualTo(1L);
        assertThat(book.get().getIsbn()).isEqualTo(isbn);

        verify(repository, times(1)).findByIsbn(isbn);

    }

}
