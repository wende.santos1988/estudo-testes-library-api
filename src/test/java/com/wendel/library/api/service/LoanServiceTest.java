package com.wendel.library.api.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import com.wendel.library.api.model.entity.Loan;
import com.wendel.library.api.repository.LoanRepository;
import com.wendel.library.api.service.impl.LoanService;
import com.wendel.library.utils.LoanUtils;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
public class LoanServiceTest {

    ILoanService service;

    @MockBean
    LoanRepository repository;

    @BeforeEach
    public void setUp() {
        this.service = new LoanService(repository);
    }

    @Test
    @DisplayName("should save a loan")
    public void shouldSaveALoan() {
        Loan loanToSave = LoanUtils.LoanWithId();

        Loan savedLoan = LoanUtils.LoanWithId();
        when(repository.save(loanToSave)).thenReturn(savedLoan);

        Loan loan = service.save(loanToSave);

        assertThat(loan.getId()).isEqualTo(loanToSave.getId());
        assertThat(loan.getBook().getId()).isEqualTo(loanToSave.getBook().getId());
        assertThat(loan.getCustomer()).isEqualTo(loanToSave.getCustomer());
        assertThat(loan.getLoanDate()).isEqualTo(loanToSave.getLoanDate());


    }
    
}
