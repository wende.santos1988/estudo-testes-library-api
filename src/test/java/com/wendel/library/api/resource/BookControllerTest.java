package com.wendel.library.api.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wendel.library.api.model.dto.BookDTO;
import com.wendel.library.api.model.entity.Book;
import com.wendel.library.api.service.IBookService;
import com.wendel.library.exceptions.BusinessException;
import com.wendel.library.utils.BookDTOUtils;
import com.wendel.library.utils.BookUtils;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Collections;
import java.util.Optional;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@WebMvcTest(controllers = BookController.class)
@AutoConfigureWebMvc
public class BookControllerTest {

    static String BOOK_API = "/api/books";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    IBookService service;

    @Test
    @DisplayName("should create book with a success")
    public void sholdCreateBookWithASuccess() throws Exception {

        //CENÁRIO
        BookDTO bookDTO = BookDTOUtils.createNewBookDTO();
        Book savedBook = Book.builder().id(10L).title("Meu Livro").author("autor").isbn("123").build();

        //EXECUÇÃO
        BDDMockito.given(service.save(Mockito.any(Book.class))).willReturn(savedBook);
        String json = new ObjectMapper().writeValueAsString(bookDTO);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(BOOK_API)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json);

        //VERIFICAÇÃO
        mockMvc
                .perform(request)
                .andExpect(status().isCreated())
                .andExpect(jsonPath("id").isNotEmpty())
                .andExpect(jsonPath("title").value(bookDTO.getTitle()))
                .andExpect(jsonPath("author").value(bookDTO.getAuthor()))
                .andExpect(jsonPath("isbn").value(bookDTO.getIsbn()));
    }

    @Test
    @DisplayName("should throw an error when the fields provided are not enough to create the book")
    public void shouldThrowAnErrorWhenTheFieldsProvidedAreNotEnoughToCreateTheBook() throws Exception {

        String json = new ObjectMapper().writeValueAsString(new BookDTO());

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(BOOK_API)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json);

        mockMvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors", hasSize(3)));
    }

    @Test
    @DisplayName("should throw an error when trying to create a book with duplicate isbn")
    public void shouldThrowAnErrorWhenTryingToCreateABookWithDuplicateIsbn() throws Exception {

        BookDTO dto = BookDTOUtils.createNewBookDTO();

        String json = new ObjectMapper().writeValueAsString(dto);
        String message = "ISBN já cadastrado.";
        BDDMockito.given(service.save(Mockito.any(Book.class)))
                .willThrow(new BusinessException(message));

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .post(BOOK_API)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .content(json);

        mockMvc.perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors", hasSize(1)))
                .andExpect(jsonPath("errors[0]").value(message));
    }

    @Test
    @DisplayName("should get information a book")
    public void shouldGetInformationABook() throws Exception {
        //Cenario (given)
        Long id = 1L;
        Book book = BookUtils.createNewBook();
        book.setId(id);
        BDDMockito.given(service.getById(id)).willReturn(Optional.of(book));

        //Execucao (when)
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get(BOOK_API.concat("/" + id))
                .accept(MediaType.APPLICATION_JSON);

        BookDTO bookDTO = BookDTOUtils.createNewBookDTO();

        // Verificacao
        mockMvc
                .perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(id))
                .andExpect(jsonPath("title").value(bookDTO.getTitle()))
                .andExpect(jsonPath("author").value(bookDTO.getAuthor()))
                .andExpect(jsonPath("isbn").value(bookDTO.getIsbn()));

    }

    @Test
    @DisplayName("should return resource not found when book not exists")
    public void shouldReturnResourceNotFoundWhenBookNotExists() throws Exception {

        // Cenario (given)
        Long id = 1L;
        BDDMockito.given(service.getById(Mockito.anyLong())).willReturn(Optional.empty());

        // Execucao (when)
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .get(BOOK_API.concat("/" + 1))
                .accept(MediaType.APPLICATION_JSON);

        // Verificacao
        mockMvc
                .perform(request)
                .andExpect(status().isNotFound());

    }

    @Test
    @DisplayName("should delete book")
    public void shoudDeleteBook() throws Exception {

        //Cenario (given)
        BDDMockito.given(service.getById(Mockito.anyLong())).willReturn(Optional.of(BookUtils.bookWithId()));

        //Execucao (when)
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete(BOOK_API.concat("/" + 1));

        // Verificacao
        mockMvc
                .perform(request)
                .andExpect(status().isNoContent());
    }

    @Test
    @DisplayName("should return resource not found when the book to delete is not found")
    public void shouldReturnRresourceNotFoundWhenTheBookToDeleteIsNotFound() throws Exception {

        //Cenario (given)
        BDDMockito.given(service.getById(Mockito.anyLong())).willReturn(Optional.empty());

        //Execucao (when)
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .delete(BOOK_API.concat("/" + 1));

        // Verificacao
        mockMvc.perform(request)
               .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("should update a book")
    public void shouldUpdateABook() throws Exception {

        //Cenario (given)
        Long id = 1L;

        Book updatingBook = BookUtils.bookWithIdToUpdate();
        Book updatedBook = BookUtils.bookWithIdToUpdate();

        String json = new ObjectMapper().writeValueAsString(updatingBook);

        BDDMockito.given(service.getById(id))
                .willReturn(Optional.of(updatingBook));


        BDDMockito.given(service.update(updatingBook)).willReturn(updatedBook);

        //Execucao (when)
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put(BOOK_API.concat("/" + 1))
                .content(json)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        // Verificacao
        mockMvc
                .perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("id").value(id))
                .andExpect(jsonPath("title").value(updatedBook.getTitle()))
                .andExpect(jsonPath("author").value(updatedBook.getAuthor()))
                .andExpect(jsonPath("isbn").value(updatedBook.getIsbn()));

    }

    @Test
    @DisplayName("should return 404 when updating a nonexistent book")
    public void shouldReturn404WhenUpdatingANonexistentBook() throws Exception {
        //Cenario (given)
        Long id = 1L;
        String json = new ObjectMapper().writeValueAsString(BookUtils.createNewBook());
        BDDMockito.given(service.getById(Mockito.anyLong()))
                .willReturn(Optional.empty());

        //Execucao (when)
        MockHttpServletRequestBuilder request = MockMvcRequestBuilders
                .put(BOOK_API.concat("/" + 1))
                .content(json)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON);

        // Verificacao
        mockMvc
                .perform(request)
                .andExpect(status().isNotFound());
    }

    @Test
    @DisplayName("should filter a book")
    public void shouldAListOfBooks() throws Exception {
        // cenario
        Long id = 1L;
        Book book = BookUtils.bookWithId();

        BDDMockito.given(service.find(Mockito.any(Book.class), Mockito.any(Pageable.class)))
            .willReturn(new PageImpl<Book>(Collections.singletonList(book), PageRequest.of(0, 100), 1));


        //Execuao
        String queryString = String.format("?title=%s&author=%spage=0&size=100"
                , book.getTitle()
                , book.getAuthor());

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.get(BOOK_API.concat(queryString))
                .accept(MediaType.APPLICATION_JSON);

        mockMvc.perform(request)
                .andExpect(status().isOk())
                .andExpect(jsonPath("content", hasSize(1)))
                .andExpect(jsonPath("totalElements").value(1))
                .andExpect(jsonPath("pageable.pageSize").value(100))
                .andExpect(jsonPath("pageable.pageNumber").value(0))
        ;

    }

}
