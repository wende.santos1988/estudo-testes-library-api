package com.wendel.library.api.resource;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.wendel.library.api.model.dto.LoanDTO;
import com.wendel.library.api.model.entity.Book;
import com.wendel.library.api.model.entity.Loan;
import com.wendel.library.api.service.IBookService;
import com.wendel.library.api.service.ILoanService;
import com.wendel.library.exceptions.BusinessException;
import com.wendel.library.utils.BookUtils;
import com.wendel.library.utils.LoanUtils;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.BDDMockito;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import java.util.Optional;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@ExtendWith(SpringExtension.class)
@ActiveProfiles("test")
@WebMvcTest(controllers = LoanController.class)
@AutoConfigureWebMvc
public class LoanControllerTest {

    static String LOAN_API = "/api/loans";

    @Autowired
    MockMvc mockMvc;

    @MockBean
    IBookService bookService;

    @MockBean
    ILoanService loanService;

    @Test
    @DisplayName("should make a loan")
    public void shouldMakeALoan() throws Exception {

        LoanDTO loanDTO = LoanDTO.builder().isbn("123").customer("Wendel").build();
        String json = new ObjectMapper().writeValueAsString(loanDTO);

        Book bookWithId = BookUtils.bookWithId();
        Loan loan = LoanUtils.LoanWithIdAndBook(bookWithId);

        BDDMockito.given(bookService.getBookByIsbn("123")).willReturn(Optional.of(bookWithId));

        BDDMockito.given(loanService.save(Mockito.any(Loan.class))).willReturn(loan);

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(LOAN_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mockMvc
                .perform(request)
                .andExpect(status().isCreated())
                .andExpect(content().string("1"));
    }

    @Test
    @DisplayName("should return error when trying make a loan with isbn invalid")
    public void shouldReturnErrorWhenTryingMakeALoanWithIsbnInvalid() throws Exception {

        LoanDTO loanDTO = LoanDTO.builder().isbn("123").customer("Wendel").build();
        String json = new ObjectMapper().writeValueAsString(loanDTO);

        BDDMockito.given(bookService.getBookByIsbn("123")).willReturn(Optional.empty());

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(LOAN_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mockMvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors", Matchers.hasSize(1)))
                .andExpect(jsonPath("errors[0]").value("Book not found for passed ISBN"));

    }

    @Test
    @DisplayName("should return error when trying make a loan in loaned book ")
    public void shouldReturnErrorWhenTryingMakeALoanInLoanedBook() throws Exception {

        LoanDTO loanDTO = LoanDTO.builder().isbn("123").customer("Wendel").build();
        String json = new ObjectMapper().writeValueAsString(loanDTO);

        Book bookWithId = BookUtils.bookWithId();

        BDDMockito.given(bookService.getBookByIsbn("123")).willReturn(Optional.of(bookWithId));

        BDDMockito.given(loanService.save(Mockito.any(Loan.class)))
                .willThrow(new BusinessException("Book already loaned"));

        MockHttpServletRequestBuilder request = MockMvcRequestBuilders.post(LOAN_API)
                .accept(MediaType.APPLICATION_JSON)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json);

        mockMvc
                .perform(request)
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("errors", Matchers.hasSize(1)))
                .andExpect(jsonPath("errors[0]").value("Book already loaned"));
    }

}
