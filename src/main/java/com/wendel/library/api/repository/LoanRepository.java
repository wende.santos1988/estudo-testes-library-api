package com.wendel.library.api.repository;

import java.util.Optional;

import com.wendel.library.api.model.entity.Loan;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LoanRepository extends JpaRepository<Loan, Long> {
    boolean existsByIsbn(String isbn);

    Optional<Loan> findByIsbn(String isbn);
}
