package com.wendel.library.api.resource;

import com.wendel.library.api.model.dto.BookDTO;
import com.wendel.library.api.model.entity.Book;
import com.wendel.library.api.service.IBookService;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/books")
public class BookController {

//    @Autowired
    private final IBookService service;

    public BookController(IBookService service) {
        this.service = service;
    }

    @Autowired
    private ModelMapper mapper;

    @GetMapping("/{id}")
    public BookDTO findById(@PathVariable Long id) {
        return service
                .getById(id)
                .map(this::converterToDTO)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public BookDTO create(@Valid @RequestBody BookDTO bookDTO) {

        Book entity = converterToEntity(bookDTO);
        entity = service.save(entity);

        return converterToDTO(entity);
    }

    @DeleteMapping("{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable Long id) {
        Book book = service
                .getById(id)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
        service.delete(book);
    }



    @PutMapping("/{id}")
    public BookDTO update(@PathVariable Long id, BookDTO bookDTO) {

        return service.getById(id).map(book -> {
            book.setAuthor(bookDTO.getAuthor());
            book.setTitle(bookDTO.getTitle());

            book = service.update(book);

            return this.converterToDTO(book);
        }).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND));
    }

    @GetMapping
    public Page<BookDTO> find(BookDTO dto, Pageable pageRequest) {
        Book filter = converterToEntity(dto);
        Page<Book> result = service.find(filter, pageRequest);
        List<BookDTO> listBookDto = result.getContent()
                .stream()
                .map(this::converterToDTO)
                .collect(Collectors.toList());
        return new PageImpl<>(listBookDto, pageRequest, result.getTotalElements());
    }


    public BookDTO converterToDTO(Book book) {
        return mapper.map(book, BookDTO.class);
    }

    public Book converterToEntity(BookDTO bookDTO) {
        return mapper.map(bookDTO, Book.class);
    }

}
