package com.wendel.library.api.resource;

import com.wendel.library.api.model.dto.LoanDTO;
import com.wendel.library.api.model.entity.Book;
import com.wendel.library.api.model.entity.Loan;
import com.wendel.library.api.service.IBookService;
import com.wendel.library.api.service.ILoanService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.time.LocalDate;

@RestController
@RequestMapping("api/loans")
public class LoanController {

    private final IBookService bookService;
    private final ILoanService loanService;

    public LoanController(IBookService bookService, ILoanService loanService) {
        this.bookService = bookService;
        this.loanService = loanService;
    }


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Long create(@RequestBody LoanDTO dto) {
        Book book = bookService
                .getBookByIsbn(dto.getIsbn())
                .orElseThrow(() ->
                        new ResponseStatusException(HttpStatus.BAD_REQUEST, "Book not found for passed ISBN"));
        Loan loan = Loan.builder()
                .book(book)
                .customer(dto.getCustomer())
                .loanDate(LocalDate.now()).build();

        Loan loanSaved = loanService.save(loan);
        return loanSaved.getId();
    }

}
