package com.wendel.library.api.service;

import com.wendel.library.api.model.entity.Loan;

public interface ILoanService {
    Loan save(Loan loan);
}
