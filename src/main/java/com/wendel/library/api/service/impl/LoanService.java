package com.wendel.library.api.service.impl;

import com.wendel.library.api.model.entity.Loan;
import com.wendel.library.api.repository.LoanRepository;
import com.wendel.library.api.service.ILoanService;
import org.springframework.stereotype.Service;

@Service
public class LoanService implements ILoanService {

    private LoanRepository repository;

    public LoanService(LoanRepository repository) {
        this.repository = repository;
    }

    @Override
    public Loan save(Loan loan) {
        return repository.save(loan);
    }
}
